Chris Pikula
200248832
ENEL 487 A3

This code is modified from supplied code for assignment 3.  The added 
function is 'void insert(data, unsigned)'.  The purose of the added 
function is to insert a new data element into position n of a linked list,
where n is the position that the new element should appear in the list,
as per starting from position 0.

testq.cpp was also modified.

The Assignment should compile with updated makefile.  
Two warnings appear as I'm doing some pointer casting.
