/**
   Project: Implementation of a Queue in C++.
   Programmer: Karim Naqvi
   Course: enel487
   Description: test code
*/

#include <iostream>
#include <cassert>
#include <cstdlib>
#include <vector>

#include "queue.h"

using namespace std;

/**
   Compare the given queue to the given array of data elements.  Return
   true if they are all equal.
 */
bool testQueueEquality( Queue& q, vector<Data> const array)
{
    if (q.size() != array.size())
	return false;

    for (unsigned i = 0; i < q.size(); ++i)
    {
        Data d(0,0);
	q.remove(&d);
	if ( !d.equals(array[i]) )
	    return false;
	q.insert(d);
    }
    return true;
}


int main(void)
{
    cout << "Testing queue.\n";
    Queue q1;
	Queue q2;

    Data d11(1,1);
    Data d33(3,3);
    Data d55(5,5);

	Data d00a(0, 0);
	Data d22a(2, 2);
	Data d44a(4, 4);
	Data d66a(6, 6);
	Data d88a(8, 8);

    q1.insert(d11);
    q1.insert(d33);
    q1.insert(d55);
    q1.print();

	q2.insert(d11);
	q2.insert(d33);
	q2.insert(d55);
	q2.print();


	q2.insert(d22a, 1);
	q2.insert(d44a, 3);
	q2.insert(d66a, 5);
	//q1.insert(d88a, 8); //should fail
	q1.print();

	q2.insert(d00a, 0);
	q2.print();

    vector<Data> dataVec;
    dataVec.push_back(d11);
    dataVec.push_back(d33);
    dataVec.push_back(d55);

    assert(testQueueEquality(q1, dataVec));

    Data d44(4, 4);
    bool found = q1.search(d44);
    assert(found == false);


    q1.insert(d44);  // now is (1,1),(3,3),(5,5),(4,4)
    found = q1.search(d44);
    assert(found == true);

    // now queue is(1,1),(3,3),(5,5),(4,4) and 
    // dataVec has (1,1),(3,3),(5,5).  Not equal
    assert(testQueueEquality(q1, dataVec) == false);

    Data temp;
    q1.remove(&temp);  // now q1 is (3,3),(5,5),(4,4)

    Data temp2(1,1);
    assert(temp.equals(temp2));  // (1,1) == (1,1)

    Data temp3(6,6);
    found = q1.search(temp3);
    assert(found == false);



	Data temp4(6, 6);
	found = q2.search(temp4);
	assert(found == true);

}

