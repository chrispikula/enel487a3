/**
   Project: Implementation of a Queue in C++.
   Programmer: Karim Naqvi
   Course: enel487
*/

#include "queue.h"
#include <iostream>
#include <cstdlib>              // for exit

using namespace std;

Queue::Queue()
{
    head = 0;
    tail = 0;
    nelements = 0;
    verbose = false;
}

Queue::~Queue()
{
    for (QElement* qe = head; qe != 0;)
    {
	QElement* temp = qe;
	qe = qe->next;
	delete(temp);
    }
}

void Queue::remove(Data* d)
{
    if (size() > 0)
    {
        QElement* qe = head;
        head = head->next;
        nelements--;
        *d = qe->data;
	delete qe;
    }
    else
    {
        cerr << "Error: Queue is empty.\n";
        exit(1);
    }
}

void Queue::insert(Data d)
{
    if (verbose) std::cout << "insert(d)\n";
    QElement* el = new QElement(d);
    if (size() > 0)
    {
        tail->next = el;
    }
    else
    {
        head = el;
    }
    tail = el;
    nelements++;
}
/****************************************************************
Insert function for A3

****************************************************************/
void Queue::insert(Data d, unsigned position)
{
	//QElement* tempHead = new QElement(d);
	int TemporaryHead = (int)head;
	//cout << "TEMPHEAD = " << TempHead << endl;
	if (verbose) std::cout << "insert(d) at position\n";
	if (position > size())
	{
		cerr << "insert: range error.\n";
		exit(3);
	}
	else
	{
		QElement* el = new QElement(d);
		if (position == 0)
		{
			el->next = head;
			head = el;
		}
		else
		{
			//tempHead->next = head;
			for (unsigned i = 0; i < (position - 1); i++)
			{
				head = head->next;  //Find our location
			}
			//Store the position of the head
			el->next = head->next;
			head->next = el;

			head = (QElement*)TemporaryHead;
			//head = tempHead->next;
			//delete tempHead;  
		}
	}
	nelements++;
	return;

}

bool Queue::search(Data otherData)
{
    QElement* insideEl = head;
    for (int i = 0; i < nelements; i++)
    {
        if (insideEl->data.equals(otherData))
            return true;
        insideEl = insideEl->next;
    }
    return false;
}

void Queue::print() const
{
    QElement* qe = head;
    if (size() > 0)
    {
        for (unsigned i = 0; i < size(); i++)
        {
            cout << i << ":(" << qe->data.x << "," << qe->data.y << ") ";
            qe = qe->next;
        }
    }
    cout << "\n";
}

unsigned Queue::size() const
{
    return nelements;
}
